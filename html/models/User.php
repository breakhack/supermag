<?php

class User {

    /**
     * Добавляет запись нового пользователя в базу данных
     * @param type $name
     * @param type $email
     * @param type $password
     * @return type boolean
     */
    public static function register($name, $email, $password) {
        $db = Db::getConnection();

        $sql = "INSERT INTO user (name, email, password, role) "
                . "VALUES (:name, :email, :password, 'user')";
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);

        return $result->execute();
    }

    public static function edit($id, $name, $password) {
        $db = Db::getConnection();

        $sql = "UPDATE user "
                . "SET name = :name, password = :password "
                . "WHERE id = :id";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);

        return $result->execute();
    }

    /**
     * Проверяет имя: не меньше чем 2 символа
     * @param type $name
     * @return boolean
     */
    public static function checkName($name) {
        if (strlen($name) >= 2) {
            return true;
        }
        return false;
    }

    /**
     * Проверяет телефон: не меньше чем 6 символов
     * @param type $phone
     * @return boolean
     */
    public static function checkPhone($phone) {
        if (strlen($phone) >= 6) {
            return true;
        }
        return false;
    }

    /**
     * Проверяет пароль: не меньше чем 8 символов
     * @param type $password
     * @return boolean
     */
    public static function checkPassword($password) {
        if (strlen($password) >= 8) {
            return true;
        }
        return false;
    }

    /**
     * Проверяет email
     * @param type $email
     * @return boolean
     */
    public static function checkEmail($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    /**
     * Проверяет email
     * @param type $email
     * @return boolean
     */
    public static function checkEmailExists($email) {
        $db = Db::getConnection();

        $sql = 'SELECT COUNT(*) FROM user WHERE email = :email ';
        $result = $db->prepare($sql);
        $result->bindparam(':email', $email, PDO::PARAM_STR);
        $result->execute();

        if ($result->fetchColumn()) {
            return true;
        }
        return false;
    }

    /**
     * Проверяет существование пользователя с заданными $email и $password
     * @param type $email
     * @param type $password
     * @return mixed: integer user id or false
     */
    public static function checkUserData($email, $password) {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM user WHERE email = :email AND password = :password';
        $result = $db->prepare($sql);
        $result->bindparam(':email', $email, PDO::PARAM_STR);
        $result->bindparam(':password', $password, PDO::PARAM_STR);
        $result->execute();

        $user = $result->fetch();

        if ($user) {
            return $user['id'];
        }
        return false;
    }

    public static function auth($userId) {
        $_SESSION['user'] = $userId;
    }

    public static function checkLogged() {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }

        header("location: /user/login");
    }

    public static function isGuest() {
        if (isset($_SESSION['user'])) {
            return false;
        }

        return true;
    }

    public static function getUserById($id) {
        if ($id) {

            $db = Db::getConnection();

            $sql = 'SELECT * FROM user WHERE id = :id';
            $result = $db->prepare($sql);
            $result->bindparam(':id', $id, PDO::PARAM_INT);
            $result->execute();

            return $result->fetch();
        }
    }

}
