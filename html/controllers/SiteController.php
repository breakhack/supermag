<?php

class SiteController
{

    public function actionIndex()
    {
        $categories = [];
        $categories = Category::getCategoriesList();

        $latestProduct = [];
        $latestProduct = Product::getLatestProducts(6);

        $recommendedProduct = [];
        $recommendedProduct = Product::getRecommendedProducts();


        require_once (ROOT . '/views/site/index.php');

        return true;
    }

    public function actionContact()
    {
        $userEmail = '';
        $userText = '';
        $result = false;

        if (isset($_POST['submit'])) {
            $userEmail = $_POST['userEmail'];
            $userText = $_POST['userText'];

            $errors = false;


            if (!User::checkEmail($userEmail)) {
                $errors[] = 'Неправильный email';
            }
            if ($errors == false) {
                $headers = 'From: thebreakhack@ustream.ga' . "\r\n" .
                        'Reply-To: thebreakhack@ustream.ga' . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();
                $adminEmail = 'anstmironov@yandex.ru';
                $message = "Тема: {$userText}\nОТ: {$userEmail}";
                $subject = 'Обратная связь http://ustream.ga';
                $result = mail($adminEmail, $subject, $message);
                $result = true;
            }
        }

        require_once(ROOT . '/views/site/contact.php');

        return true;
    }

    public function actionAbout()
    {
        require_once(ROOT . '/views/site/about.php');

        return true;
    }

    public function action404()
    {
        require_once(ROOT . '/views/site/404.php');

        return true;
    }

}
