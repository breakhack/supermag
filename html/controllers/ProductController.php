<?php

class ProductController
{

    public function actionView($productId)
    {
        $categories = [];
        $categories = Category::getCategoriesList();
        
        $product = Product::getProductId($productId);
//        echo '<pre>';
//        print_r($product);
//        echo '</pre>';
        require_once (ROOT . '/views/product/view.php');

        return true;
    }

}
